import proxy.ProxyServer

const val SERVER_PORT = 1337

fun main() {
    ProxyServer(SERVER_PORT).startServer()
}