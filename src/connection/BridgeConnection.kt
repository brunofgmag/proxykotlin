package connection

import java.io.File
import java.lang.NumberFormatException
import java.net.Socket
import java.net.SocketException
import java.net.SocketTimeoutException
import java.security.MessageDigest
import kotlin.concurrent.thread

class BridgeConnection(private val clientSocket : Socket,
                       private var serverSocket : Socket,
                       private val clientRequest : StringBuilder,
                       private val keepAlive : Boolean) : Runnable {
    private val clientInput = clientSocket.getInputStream()
    private val clientOutput = clientSocket.getOutputStream()
    private var serverInput = serverSocket.getInputStream()
    private var serverOutput = serverSocket.getOutputStream()
    private val bufferSize = clientSocket.receiveBufferSize
    private val header = StringBuilder()
    private val cacheAgeRegex = "Cache-Control: max-age=(.*)".toRegex()
    private val etageRegex = "Etag: (.*)".toRegex()
    private var cacheAge = "0;"
    private var etag = ";"
    private val currentTime = "${System.currentTimeMillis()}\n"
    private val req = clientRequest.toString().split("\r\n")[0]
    private val cacheFile = File("cache/", "${md5(req)}.txt")

    init {
        serverSocket.receiveBufferSize = bufferSize
        serverSocket.sendBufferSize = bufferSize
    }

    override fun run() {
        println("Conetado ao servidor via bridge: ${serverSocket.remoteSocketAddress}.")
        Thread(Bridge()).start()
    }

    private fun md5(toEncrypt: String): String {
        val digest = MessageDigest.getInstance("md5")
        digest.update(toEncrypt.toByteArray())
        val bytes = digest.digest()
        val sb = StringBuilder()

        for (i in bytes.indices) {
            sb.append(String.format("%02X", bytes[i]))
        }

        return sb.toString().toLowerCase()
    }

    private fun closeConnections() {
        println("Fechando conexão ${clientSocket.remoteSocketAddress} $req.")
        println("Fechando conexão ${serverSocket.remoteSocketAddress} $req.")
        serverInput.close()
        serverOutput.close()
        serverSocket.close()
        clientInput.close()
        clientOutput.close()
        clientSocket.close()
    }

    private fun writeToCache(dataCache : ArrayList<Byte>) {
        if(dataCache.isEmpty()) {
            return
        }

        val rawHeader = StringBuilder()
        dataCache.forEach {
            rawHeader.append(it.toChar())

            if(rawHeader.contains("\r\n\r\n")) {
                return@forEach
            }
        }

        if(cacheAgeRegex.containsMatchIn(rawHeader)) {
            cacheAge = "${cacheAgeRegex.find(rawHeader)!!.groups[1]!!.value};"
        } else {
            return
        }

        if(etageRegex.containsMatchIn(rawHeader)) {
            etag = "${etageRegex.find(rawHeader)!!.groups[1]!!.value};"
        }

        addDataHeader(dataCache)

        if(!validateCache()) {
            println("Apagando arquivo de cache da conexão ${serverSocket.remoteSocketAddress} $req pois o mesmo está expirado.")
            cacheFile.delete()
        }

        if(!cacheFile.exists()) {
            println("Escrevendo cache para ${serverSocket.remoteSocketAddress} $req.")

            cacheFile.createNewFile()
            cacheFile.writeBytes(dataCache.toByteArray())
        }
    }

    private fun fetchHeader(dataCache : ArrayList<Byte>) {
        dataCache.forEach {
            header.append(it.toChar())

            if(header.contains("\r\n\r\n")) {
                return@forEach
            }
        }
    }

    private fun addDataHeader(dataCache : ArrayList<Byte>) {
        val dataTemp = ArrayList<Byte>()

        etag.forEach {
            dataTemp.add(it.toByte())
        }

        cacheAge.forEach {
            dataTemp.add(it.toByte())
        }

        currentTime.forEach {
            dataTemp.add(it.toByte())
        }

        dataTemp.addAll(dataCache)
        dataCache.clear()
        dataCache.addAll(dataTemp)

        fetchHeader(dataCache)
    }

    private fun validateCache() : Boolean {
        val data = header.split("\n")[0]
        val split = data.split(";")

        val timestamp = split[2].split("\n")[0]
        val cacheTime = split[1]
        val fileEtag = split[0]

        if(etag == ";") {
            etag = ""
        }

        try {
            if(timestamp.toLong() + cacheTime.toInt() < System.currentTimeMillis() || fileEtag != etag) {
                return false
            }
        } catch(e : NumberFormatException) {
            return false
        }

        return true
    }

    private fun sendRequest() {
        println("Executando requisição ${serverSocket.remoteSocketAddress} $req.")

        serverOutput.write(clientRequest.toString().toByteArray())
        serverOutput.flush()
    }

    private fun beginTransaction() {
        val buffer = ByteArray(8)
        val cacheData = ArrayList<Byte>()
        var remoteData = false

        if(cacheFile.exists()) {
            println("Lendo do cache para a conexão ${serverSocket.remoteSocketAddress} $req.")
            serverInput = cacheFile.inputStream()

            do{
                val byte = serverInput.read()
            } while(byte.toChar() != '\n')
        } else {
            remoteData = true
        }

        if(remoteData) {
            sendRequest()
        }

        do {
            try {
                val bytes = serverInput.read(buffer)
                if(bytes > 0) {
                    buffer.forEach {
                        cacheData.add(it)
                    }
                    clientOutput.write(buffer, 0, buffer.size)
                    clientOutput.flush()
                }
            } catch (e : SocketException) {
                if(!keepAlive) {
                    closeConnections()
                    return
                }
            } catch (e1 : SocketTimeoutException) {
                writeToCache(cacheData)

                if(!keepAlive) {
                    closeConnections()
                    return
                }
            }
        } while(serverInput.available() > 0)

        thread(start = true) {
            writeToCache(cacheData)
        }
    }

    private inner class Bridge : Runnable {
        override fun run() {
            beginTransaction()

            if(keepAlive) {
                try {
                    clientOutput.write("\r\n0\r\n".toByteArray())
                    clientOutput.flush()
                } catch (e: SocketException) {
                    closeConnections()
                }
            } else {
                closeConnections()
            }
        }
    }
}