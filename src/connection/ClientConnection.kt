package connection

import proxy.ProxyType
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.net.*
import java.text.SimpleDateFormat
import java.util.*

class ClientConnection(private val clientSocket : Socket) : Runnable {
    private var inputStream : InputStream
    private var outputStream : OutputStream

    init {
        println("Recebeu conexão de um cliente: ${clientSocket.remoteSocketAddress}.")
        inputStream = clientSocket.getInputStream()
        outputStream = clientSocket.getOutputStream()
        Thread(this).start()
    }

    override fun run() {
        startConnection()
    }

    private fun setBufferSize(size : Int) {
        clientSocket.sendBufferSize = size
        clientSocket.receiveBufferSize = size
    }

    private fun startConnection() {
        val inputReader = inputStream.bufferedReader()
        val clientRequest = StringBuilder()
        val blockedDomains = File("res/blocked_domains.txt")
        val blockedWords = File("res/blocked_words.txt")
        val blockedIPs = File("res/blocked_ips.txt")

        do {
            var requestLine: String
            try {
                requestLine = inputReader.readLine().plus("\r\n")
                clientRequest.append(requestLine)
            } catch (e : SocketTimeoutException) {
                println("ERRO! Socket timeout. Fechando conexão ${clientSocket.remoteSocketAddress}.")
                inputReader.close()
                closeConnection()
                return
            }
        } while (requestLine != "\r\n" && requestLine != "null\r\n")

        if(clientRequest.toString() == "\r\n" || clientRequest.toString() == "null\r\n") {
            println("ERRO! O cliente não enviou nenhuma requisição. Fechando conexão ${clientSocket.remoteSocketAddress}.")
            inputReader.close()
            closeConnection()
            return
        }

        val hostRegex = "Host: (.*)".toRegex()
        val connectRegex = "CONNECT (.*):(.*) ".toRegex()
        val keepAliveRegex = "Keep-Alive: timeout=(.*) ".toRegex()
        var keepAlive = false
        var serverHost = ""
        var serverPort = 80
        var proxyType = ProxyType.BRIDGE

        if(hostRegex.containsMatchIn(clientRequest)) {
            serverHost = hostRegex.find(clientRequest)!!.groups[1]!!.value
            keepAlive = clientRequest.toString().toLowerCase().contains("connection: keep-alive")

            setBufferSize(256)
        }

        if(connectRegex.containsMatchIn(clientRequest)) {
            serverHost = connectRegex.find(clientRequest)!!.groups[1]!!.value
            serverPort = connectRegex.find(clientRequest)!!.groups[2]!!.value.toInt()
            proxyType = ProxyType.TUNNELING
            keepAlive = true

            outputStream.write("200 OK".toByteArray())
            outputStream.flush()

            setBufferSize(8192)
        }

        blockedDomains.readText().split("\r\n").forEach {
            if(serverHost == it) {
                println("Conexão negada para $serverHost.")
                closeConnection()
                return
            }
        }

        blockedWords.readText().split("\r\n").forEach {
            if(serverHost.contains(it) && it != "") {
                println("Conexão negada para $serverHost.")
                closeConnection()
                return
            }
        }

        blockedIPs.readText().split("\r\n").forEach {
            val split = it.split(";")
            if(clientSocket.remoteSocketAddress.toString().split(":")[0].split("/")[1] == split[0].split(":")[0]) {
                val time = SimpleDateFormat("HHmm").format(Date())
                if(time.toInt() >= split[1].toInt()  && time.toInt() <= split[2].toInt() ) {
                    println("Conexão negada para ${clientSocket.remoteSocketAddress}.")
                    closeConnection()
                    return
                }
            }
        }

        try {
            val serverSocket = Socket(InetAddress.getByName(serverHost), serverPort)

            if(proxyType == ProxyType.BRIDGE) {
                if(keepAliveRegex.containsMatchIn(clientRequest)) {
                    serverSocket.soTimeout = connectRegex.find(clientRequest)!!.groups[1]!!.value.toInt()
                    clientSocket.soTimeout = 3000
                } else {
                    serverSocket.soTimeout = 3000
                    clientSocket.soTimeout = 3000
                }

                Thread(BridgeConnection(clientSocket, serverSocket, clientRequest, keepAlive)).start()
            } else {
                Thread(TunnelingConnection(clientSocket, serverSocket, clientRequest)).start()
            }

        } catch (e : UnknownHostException) {
            println("ERRO! O host $serverHost não foi encontrado. Fechando conexão ${clientSocket.remoteSocketAddress}.")
            closeConnection()
        } catch (e1 : ConnectException) {
            println("ERRO! O host $serverHost demorou demais a responder. Fechando conexão ${clientSocket.remoteSocketAddress}.")
        }
    }

    private fun closeConnection() {
        outputStream.close()
        clientSocket.close()
    }
}