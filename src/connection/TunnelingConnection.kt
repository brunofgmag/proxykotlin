package connection

import java.net.Socket
import java.net.SocketException

class TunnelingConnection(private val clientSocket : Socket,
                          private var serverSocket : Socket,
                          clientRequest : StringBuilder) : Runnable {
    private val clientInput = clientSocket.getInputStream()
    private val clientOutput = clientSocket.getOutputStream()
    private var serverInput = serverSocket.getInputStream()
    private var serverOutput = serverSocket.getOutputStream()
    private val bufferSize = clientSocket.receiveBufferSize
    private val req = clientRequest.toString().split("\r\n")[0]

    init {
        serverSocket.receiveBufferSize = bufferSize
        serverSocket.sendBufferSize = bufferSize
    }

    override fun run() {
        println("Conetado ao servidor via tunneling: ${serverSocket.remoteSocketAddress}.")
        Thread(ServerClient()).start()
        Thread(ClientServer()).start()
    }

    private fun closeConnections() {
        println("Fechando conexão ${clientSocket.remoteSocketAddress} $req.")
        println("Fechando conexão ${serverSocket.remoteSocketAddress} $req.")
        serverInput.close()
        serverOutput.close()
        serverSocket.close()
        clientInput.close()
        clientOutput.close()
        clientSocket.close()
    }

    private inner class ServerClient : Runnable {
        private val buffer = ByteArray(bufferSize)

        override fun run() {
            do {
                var bytes: Int
                try {
                    bytes = serverInput.read(buffer)
                    if(bytes > 0) {
                        clientOutput.write(buffer, 0, bytes)
                        clientOutput.flush()
                    }
                } catch (e : SocketException) {
                    closeConnections()
                    return
                }
            } while(bytes > 0)

            closeConnections()
        }
    }

    private inner class ClientServer : Runnable {
        private val buffer = ByteArray(bufferSize)

        override fun run() {
            do {
                var bytes: Int
                try {
                    bytes = clientInput.read(buffer)
                    if(bytes > 0) {
                        serverOutput.write(buffer, 0, bytes)
                        serverOutput.flush()
                    }
                } catch (e : SocketException) {
                    closeConnections()
                    return
                }
            } while(bytes > 0)

            closeConnections()
        }
    }
}