package proxy

import connection.ClientConnection
import java.net.ServerSocket

enum class ProxyType {
    TUNNELING,
    BRIDGE
}

class ProxyServer(port : Int) {
    private val serverSocket = ServerSocket(port)
    private var running = true

    init {
        println("Servidor iniciado na porta $port.")
    }

    fun startServer() {
        serverSocket.use {
            while(running) {
                println("Aguardando novas conexões...")
                ClientConnection(it.accept())
            }
        }
    }
}